# Temporary Azure WireGuard VPN

Temporarily bring up a virtual machine in Azure and route traffic through WireGuard VPN via the virtual machine. Works on Linux and Windows.

## Use Cases

This Software is useful for:

- Changing the IPv4 Internet egress location
- Accessing the Internet using regular IPv4 NAT in case the ISP is only providing DS-Lite with crippled IPv4 access via CG-NAT (some applications like [Torchlight II](https://en.wikipedia.org/wiki/Torchlight_II) do not play well with DS-Lite / CG-NAT)
- Accessing IPv6-only hosts when only IPv4 is available
- Works on Linux and Windows (tested with Ubuntu 20.04 and Windows 8.1)

## Prerequisites

- [WireGuard](https://www.wireguard.com/install/)
- [Azure Subscription](https://azure.microsoft.com/)
- [Service Principal](https://docs.microsoft.com/en-us/azure/developer/go/azure-sdk-qs-vm)

## Installation

Pre-built:
1. Download release and extract

From source:
1. Build from source
2. Copy the executable and the ``template`` directory. For Windows additionally copy the ``.exe.manifest`` file.
3. Copy ``quickstart.auth.dist`` to ``quickstart.auth``
4. Copy ``temporary-azure-wireguard-vpn.env.dist`` to ``temporary-azure-wireguard-vpn.env``

## Usage

1. Put service principal details into ``quickstart.auth``
2. Configuration is set via file ``temporary-azure-wireguard-vpn.env`` or via environment variables. At the very minimum set ``AZ_SUBSCRIPTION_ID``
3. Run executable, wait until resources are built and VPN connection is established
4. Confirm once you are done and the VPN will be torn down and the created Azure resources will be deleted

Variable ``RESOURCE_GROUP_LOCATION`` controls in which region the resources are created and by extension this also determines the location of the IP address. Set this to a location close to you or set this to a specific region that you want to use for Internet egress.

## Restrictions

- Changing the IPv6 Internet egress location is not expected to be supported. This tool is using an ULA to connect to IPv6 Internet and anything else (GUA-GUA, IPv4-IPv4) will be preferred. Microsoft Azure only provides one single IPv6 address per server. Adapting for other hosts like Hetzner, who provide a /64 GUA prefix per server, could allow changing IPv6 egress location.
- The WireGuard server private key is transferred via [virtual machine custom data](https://docs.microsoft.com/en-us/azure/virtual-machines/custom-data) and Microsoft advises "not to store sensitive data in custom data"

## Roadmap

- IPv6 (GUA, needs support for other cloud hosts, not supported by Azure)
- IPv4 port forwarding
