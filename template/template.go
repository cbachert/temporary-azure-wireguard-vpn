// SPDX-License-Identifier: GPL-3.0-or-later

package template

import (
	"log"
	"os"
	"text/template"
)

func TemplateFile(templatePath, configPath string, config map[string]string) {
	f, err := os.Create(configPath)
	if err != nil {
		log.Fatalf("Failed to create config file: %v", err)
	}
	log.Printf("Created config file: %v", configPath)
	defer f.Close()

	t, err := template.ParseFiles(templatePath)
	if err != nil {
		log.Fatalf("Failed to parse template: %v", err)
	}
	log.Printf("Parsed template: %v", templatePath)

	t.Execute(f, config)
}
