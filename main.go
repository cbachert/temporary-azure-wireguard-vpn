// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"

	armcompute "github.com/Azure/azure-sdk-for-go/profiles/latest/compute/mgmt/compute"
	armnetwork "github.com/Azure/azure-sdk-for-go/services/network/mgmt/2021-03-01/network"
	armresources "github.com/Azure/azure-sdk-for-go/services/resources/mgmt/2020-10-01/resources"
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/azure"
	"github.com/Azure/go-autorest/autorest/azure/auth"
	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh"

	"gitlab.com/cbachert/temporary-azure-wireguard-vpn/compute"
	"gitlab.com/cbachert/temporary-azure-wireguard-vpn/network"
	"gitlab.com/cbachert/temporary-azure-wireguard-vpn/resources"
	"gitlab.com/cbachert/temporary-azure-wireguard-vpn/template"
	"gitlab.com/cbachert/temporary-azure-wireguard-vpn/wireguard"
)

type Config struct {
	AzureAuthLocation     string `mapstructure:"AZURE_AUTH_LOCATION"`
	AzPrefix              string `mapstructure:"AZ_PREFIX"`
	AzSubscriptionID      string `mapstructure:"AZ_SUBSCRIPTION_ID"`
	UserAgent             string `mapstructure:"USER_AGENT"`
	ResourceGroupName     string `mapstructure:"RESOURCE_GROUP_NAME"`
	ResourceGroupLocation string `mapstructure:"RESOURCE_GROUP_LOCATION"`
	VirtualNetworkName    string `mapstructure:"VNET_NAME"`
	SubnetName            string `mapstructure:"SUBNET_NAME"`
	NsgName               string `mapstructure:"NSG_NAME"`
	Ip4Name               string `mapstructure:"IP4_NAME"`
	Ip6Name               string `mapstructure:"IP6_NAME"`
	NicName               string `mapstructure:"NIC_NAME"`
	VmName                string `mapstructure:"VM_NAME"`
	Username              string `mapstructure:"USER_NAME"`
	VmPublisher           string `mapstructure:"VM_PUBLISHER"`
	VmOffer               string `mapstructure:"VM_OFFER"`
	VmSku                 string `mapstructure:"VM_SKU"`
	WgServerCIDR4         string `mapstructure:"WG_SERVER_CIDR4"`
	WgServerCIDR6         string `mapstructure:"WG_SERVER_CIDR6"`
	WgServerIP4           net.IP
	WgServerIP6           net.IP
	WgClientCIDR4         string `mapstructure:"WG_CLIENT_CIDR4"`
	WgClientCIDR6         string `mapstructure:"WG_CLIENT_CIDR6"`
	WgClientIP4           net.IP
	WgClientIP6           net.IP
	WgAwaitTunnelTimeStr  string `mapstructure:"WG_AWAITTUNNELTIME"`
	WgAwaitTunnelTime     int
	//WgServerIPNet         *net.IPNet
	WgIfName              string `mapstructure:"WG_IF_NAME"`
	WgConfigPath          string
	WgTemplatePath        string
	WgServerPrivateKey    string
	WgServerPublicKey     string
	WgClientPrivateKey    string
	WgClientPublicKey     string
	CloudInitTemplatePath string
	CloudInitPath         string
	SSHKeyPath            string
	SSHPubKeyPath         string
}

var (
	config     Config
	ctx        = context.Background()
	authorizer autorest.Authorizer
)

// LoadConfig reads configuration from file or environment variables.
func LoadConfig(configPath, configName, configType string) (config Config, err error) {
	viper.AddConfigPath(configPath)
	viper.SetConfigName(configName)
	viper.SetConfigType(configType)

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}

// https://stackoverflow.com/questions/21151714/go-generate-an-ssh-public-key
func GenereateSSHKeyPair() (string, string, error) {
	sshKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return "", "", err
	}

	var sshKeyBuf strings.Builder
	var sshPubKeyBuf strings.Builder

	sshKeyPEM := &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(sshKey)}
	if err := pem.Encode(&sshKeyBuf, sshKeyPEM); err != nil {
		return "", "", err
	}

	sshPubKey, err := ssh.NewPublicKey(&sshKey.PublicKey)
	if err != nil {
		return "", "", err
	}

	sshPubKeyBuf.Write(ssh.MarshalAuthorizedKey(sshPubKey))

	return sshPubKeyBuf.String(), sshKeyBuf.String(), nil
}

func CreateAzResources() (armresources.Group, armnetwork.VirtualNetwork, armnetwork.SecurityGroup, armnetwork.PublicIPAddress, armnetwork.PublicIPAddress, armnetwork.Interface, armcompute.VirtualMachine) {
	group, err := resources.CreateGroup(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, config.ResourceGroupName, config.ResourceGroupLocation)
	if err != nil {
		log.Fatalf("failed to create group: %v", err)
	}
	log.Printf("Created group: %v", *group.Name)

	vnet, err := network.CreateVirtualNetworkAndSubnet(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, config.ResourceGroupName, config.ResourceGroupLocation, config.VirtualNetworkName, config.SubnetName)
	if err != nil {
		log.Fatalf("failed to create vnet: %v", err)
	}
	log.Printf("Created vnet: %v", *vnet.Name)

	nsg, err := network.CreateNetworkSecurityGroup(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, config.ResourceGroupName, config.ResourceGroupLocation, config.NsgName)
	if err != nil {
		log.Fatalf("failed to create nsg: %v", err)
	}
	log.Printf("Created nsg: %v", *nsg.Name)

	pip4, err := network.CreatePublicIP(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, config.ResourceGroupName, config.ResourceGroupLocation, config.Ip4Name, "IPv4")
	if err != nil {
		log.Fatalf("failed to create pip: %v", err)
	}
	log.Printf("Created pip: %v %v", *pip4.Name, *pip4.IPAddress)

	pip6, err := network.CreatePublicIP(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, config.ResourceGroupName, config.ResourceGroupLocation, config.Ip6Name, "IPv6")
	if err != nil {
		log.Fatalf("failed to create pip: %v", err)
	}
	log.Printf("Created pip: %v %v", *pip6.Name, *pip6.IPAddress)

	nic, err := network.CreateNIC(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, config.ResourceGroupName, config.ResourceGroupLocation, *vnet.Name, config.SubnetName, config.NicName, nsg, pip4, pip6)
	if err != nil {
		log.Fatalf("failed to create nic: %v", err)
	}
	log.Printf("Created nic: %v", *nic.Name)

	vm, err := compute.CreateVM(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, config.ResourceGroupName, config.ResourceGroupLocation, config.VmName, config.Username, config.SSHPubKeyPath, config.VmPublisher, config.VmOffer, config.VmSku, config.CloudInitPath, nic)
	if err != nil {
		log.Fatalf("failed to create vm: %v", err)
	}
	log.Printf("Created vm: %v", *vm.Name)

	return group, vnet, nsg, pip4, pip6, nic, vm
}

func DeleteAzResources(group armresources.Group) {
	result, err := resources.DeleteGroup(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, config.ResourceGroupName)
	if err != nil {
		log.Fatalf("Failed to request removal of group and resources: %v", err)
	}
	log.Printf("Requested removal of group and resources: %v", *group.Name)

	wg := new(sync.WaitGroup)
	resources.WaitForDeleteCompletion(ctx, config.UserAgent, config.AzSubscriptionID, authorizer, wg, []armresources.GroupsDeleteFuture{result}, []string{*group.Name})
	wg.Wait()
}

func printLicenseNotice() {
	fmt.Println(`
	Temporary Azure WireGuard VPN  Copyright (C) 2021  Christian Bachert
	Azure SDK for Go Samples  Copyright (c) Microsoft Corporation
	This program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE
	This is free software, and you are welcome to redistribute it
	under certain conditions; see LICENSE for details.)
	`)
}

func main() {
	//defer cancel()
	//defer resources.Cleanup(ctx)

	printLicenseNotice()

	configPath := "."
	configName := "temporary-azure-wireguard-vpn"
	configType := "env"

	var err error

	logFile, err := os.OpenFile("log.txt", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		log.Fatalf("Cannot create log file: %v", err)
	}
	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)

	config, err = LoadConfig(configPath, configName, configType)
	if err != nil {
		log.Fatalf("Cannot load config: %v", err)
	}
	log.Printf("Loaded config: %v", configName+"."+configType)

	config.ResourceGroupName = config.AzPrefix + config.ResourceGroupName
	config.VirtualNetworkName = config.AzPrefix + config.VirtualNetworkName
	config.SubnetName = config.AzPrefix + config.SubnetName
	config.NsgName = config.AzPrefix + config.NsgName
	config.Ip4Name = config.AzPrefix + config.Ip4Name
	config.Ip6Name = config.AzPrefix + config.Ip6Name
	config.NicName = config.AzPrefix + config.NicName
	config.VmName = config.AzPrefix + config.VmName

	config.WgServerIP4, _, err = net.ParseCIDR(config.WgServerCIDR4)
	if err != nil {
		log.Fatalf("Failed to parse WireGuard server IPv4 CIDR: %v", err)
	}
	config.WgClientIP4, _, err = net.ParseCIDR(config.WgClientCIDR4)
	if err != nil {
		log.Fatalf("Failed to parse WireGuard client IPv4 CIDR: %v", err)
	}
	config.WgServerIP6, _, err = net.ParseCIDR(config.WgServerCIDR6)
	if err != nil {
		log.Fatalf("Failed to parse WireGuard server IPv6 CIDR: %v", err)
	}
	config.WgClientIP6, _, err = net.ParseCIDR(config.WgClientCIDR6)
	if err != nil {
		log.Fatalf("Failed to parse WireGuard client IPv4 CIDR: %v", err)
	}
	config.WgAwaitTunnelTime, err = strconv.Atoi(config.WgAwaitTunnelTimeStr)
	if err != nil {
		log.Fatalf("Failed to parse WireGuard wait for tunnel time: %v", err)
	}

	config.WgTemplatePath = "./template/wgaz0.conf.template"
	config.WgConfigPath = "./out/wgaz0.conf"
	config.CloudInitTemplatePath = "./template/cloud-init.txt.template"
	config.CloudInitPath = "./out/cloud-init.txt"
	config.SSHPubKeyPath = "./out/id_rsa.pub"
	config.SSHKeyPath = "./out/id_rsa"

	if _, err := os.Stat("./out"); os.IsNotExist(err) {
		err = os.Mkdir("./out", 0775)
		if err != nil {
			log.Fatalf("Cannot create ./out directory: %v", err)
		}
		log.Printf("Created ./out directory")
	}

	os.Setenv("AZURE_AUTH_LOCATION", config.AzureAuthLocation)
	// Authenticate with the Azure services using file-based authentication
	authorizer, err = auth.NewAuthorizerFromFile(azure.PublicCloud.ResourceManagerEndpoint)
	if err != nil {
		log.Fatalf("Failed to get OAuth config: %v", err)
	}

	config.WgServerPrivateKey, config.WgServerPublicKey = wireguard.GenKeyPair()
	config.WgClientPrivateKey, config.WgClientPublicKey = wireguard.GenKeyPair()

	sshPubKey, sshKey, _ := GenereateSSHKeyPair()
	ioutil.WriteFile(config.SSHPubKeyPath, []byte(sshPubKey), 0655)
	ioutil.WriteFile(config.SSHKeyPath, []byte(sshKey), 0655)
	log.Println("Generated SSH key pair")

	cloudInitConfig := map[string]string{
		"WgAddress4":        config.WgServerCIDR4,
		"WgAddress6":        config.WgServerCIDR6,
		"WgPeerAllowedIPs4": config.WgClientIP4.String() + "/32",
		"WgPeerAllowedIPs6": config.WgClientIP6.String() + "/128",
		"WgPrivateKey":      config.WgServerPrivateKey,
		"WgPublicKey":       config.WgServerPublicKey,
		"WgPeerPublicKey":   config.WgClientPublicKey,
	}
	template.TemplateFile(config.CloudInitTemplatePath, config.CloudInitPath, cloudInitConfig)

	group, _, _, pip4, _, _, _ := CreateAzResources()
	wireguard.WireguardConfig(config.WgTemplatePath, config.WgConfigPath, config.WgClientCIDR4, config.WgClientCIDR6, *pip4.IPAddress, config.WgClientPrivateKey, config.WgClientPublicKey, config.WgServerPublicKey)
	wireguard.WireguardUp(config.WgConfigPath, group, pip4, config.WgIfName)
	err = wireguard.WgAwaitTunnel(config.WgServerIP4, config.WgAwaitTunnelTime)
	if err == nil {
		fmt.Print("Press the Enter Key to stop VPN")
		fmt.Scanln()
	}
	wireguard.WireguardDown(config.WgConfigPath, group, pip4, config.WgIfName)
	DeleteAzResources(group)
}
