// SPDX-License-Identifier: MIT

// Copyright (c) Microsoft and contributors.  All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package resources

import (
	"context"
	"fmt"
	"log"
	"sync"

	"github.com/Azure/azure-sdk-for-go/profiles/latest/resources/mgmt/resources"
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/to"
)

func getGroupsClient(userAgent, subscriptionID string, authorizer autorest.Authorizer) resources.GroupsClient {
	groupsClient := resources.NewGroupsClient(subscriptionID)
	groupsClient.Authorizer = authorizer
	groupsClient.AddToUserAgent(userAgent)
	return groupsClient
}

// Create a resource group for the deployment.
func CreateGroup(ctx context.Context, userAgent, subscriptionID string, authorizer autorest.Authorizer, resourceGroupName, resourceGroupLocation string) (group resources.Group, err error) {
	groupsClient := getGroupsClient(userAgent, subscriptionID, authorizer)

	return groupsClient.CreateOrUpdate(
		ctx,
		resourceGroupName,
		resources.Group{
			Location: to.StringPtr(resourceGroupLocation)})
}

// DeleteGroup removes the resource group named by env var
func DeleteGroup(ctx context.Context, userAgent, subscriptionID string, authorizer autorest.Authorizer, resourceGroupName string) (result resources.GroupsDeleteFuture, err error) {
	groupsClient := getGroupsClient(userAgent, subscriptionID, authorizer)
	return (groupsClient.Delete(ctx, resourceGroupName))
}

// WaitForDeleteCompletion concurrently waits for delete group operations to finish
func WaitForDeleteCompletion(ctx context.Context, userAgent, subscriptionID string, authorizer autorest.Authorizer, wg *sync.WaitGroup, futures []resources.GroupsDeleteFuture, groups []string) {
	groupsClient := getGroupsClient(userAgent, subscriptionID, authorizer)

	for i, f := range futures {
		wg.Add(1)
		go func(ctx context.Context, future resources.GroupsDeleteFuture, rg string) {
			err := future.WaitForCompletionRef(ctx, groupsClient.Client)
			if err != nil {
				log.Fatalf("got error: %s", err)
			} else {
				fmt.Printf("finished deleting group '%s'\n", rg)
			}
			wg.Done()
		}(ctx, f, groups[i])
	}
}
