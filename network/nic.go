// SPDX-License-Identifier: MIT

// Copyright (c) Microsoft and contributors.  All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package network

import (
	"context"
	"fmt"
	"log"

	"github.com/Azure/azure-sdk-for-go/profiles/latest/network/mgmt/network"
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/to"
)

func getNicClient(userAgent, subscriptionID string, authorizer autorest.Authorizer) network.InterfacesClient {
	nicClient := network.NewInterfacesClient(subscriptionID)
	nicClient.Authorizer = authorizer
	nicClient.AddToUserAgent(userAgent)
	return nicClient
}

// CreateNIC creates a new network interface. The Network Security Group is not a required parameter
func CreateNIC(ctx context.Context, userAgent, subscriptionID string, authorizer autorest.Authorizer, resourceGroupName, resourceGroupLocation, vnetName, subnetName, nicName string, nsg network.SecurityGroup, pip4 network.PublicIPAddress, pip6 network.PublicIPAddress) (nic network.Interface, err error) {
	subnet, err := GetVirtualNetworkSubnet(ctx, userAgent, subscriptionID, authorizer, resourceGroupName, resourceGroupLocation, vnetName, subnetName)
	if err != nil {
		log.Fatalf("failed to get subnet: %v", err)
	}

	nicParams := network.Interface{
		Name:     to.StringPtr(nicName),
		Location: to.StringPtr(resourceGroupLocation),
		InterfacePropertiesFormat: &network.InterfacePropertiesFormat{
			IPConfigurations: &[]network.InterfaceIPConfiguration{
				{
					Name: to.StringPtr("ipConfigv4"),
					InterfaceIPConfigurationPropertiesFormat: &network.InterfaceIPConfigurationPropertiesFormat{
						PrivateIPAddressVersion:   "IPv4",
						Subnet:                    &subnet,
						PrivateIPAllocationMethod: network.IPAllocationMethodDynamic,
						PublicIPAddress:           &pip4,
					},
				},
				{
					Name: to.StringPtr("ipConfigv6"),
					InterfaceIPConfigurationPropertiesFormat: &network.InterfaceIPConfigurationPropertiesFormat{
						PrivateIPAddressVersion:   "IPv6",
						Subnet:                    &subnet,
						PrivateIPAllocationMethod: network.IPAllocationMethodDynamic,
						PublicIPAddress:           &pip6,
					},
				},
			},
		},
	}

	nicParams.NetworkSecurityGroup = &nsg

	nicClient := getNicClient(userAgent, subscriptionID, authorizer)
	future, err := nicClient.CreateOrUpdate(ctx, resourceGroupName, nicName, nicParams)
	if err != nil {
		return nic, fmt.Errorf("cannot create nic: %v", err)
	}

	err = future.WaitForCompletionRef(ctx, nicClient.Client)
	if err != nil {
		return nic, fmt.Errorf("cannot get nic create or update future response: %v", err)
	}

	return future.Result(nicClient)
}
