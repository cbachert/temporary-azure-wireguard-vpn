// SPDX-License-Identifier: MIT

// Copyright (c) Microsoft and contributors.  All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package network

import (
	"context"
	"fmt"

	"github.com/Azure/azure-sdk-for-go/profiles/latest/network/mgmt/network"
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/to"
)

func getVnetClient(userAgent, subscriptionID string, authorizer autorest.Authorizer) network.VirtualNetworksClient {
	vnetClient := network.NewVirtualNetworksClient(subscriptionID)
	vnetClient.Authorizer = authorizer
	vnetClient.AddToUserAgent(userAgent)
	return vnetClient
}

// CreateVirtualNetworkAndSubnets creates a virtual network with one subnet
func CreateVirtualNetworkAndSubnet(ctx context.Context, userAgent, subscriptionID string, authorizer autorest.Authorizer, resourceGroupName, resourceGroupLocation, vnetName, subnetName string) (vnet network.VirtualNetwork, err error) {
	vnetClient := getVnetClient(userAgent, subscriptionID, authorizer)
	future, err := vnetClient.CreateOrUpdate(
		ctx,
		resourceGroupName,
		vnetName,
		network.VirtualNetwork{
			Location: to.StringPtr(resourceGroupLocation),
			VirtualNetworkPropertiesFormat: &network.VirtualNetworkPropertiesFormat{
				AddressSpace: &network.AddressSpace{
					AddressPrefixes: &[]string{"10.0.0.0/8", "fd00::/56"},
				},
				Subnets: &[]network.Subnet{
					{
						Name: to.StringPtr(subnetName),
						SubnetPropertiesFormat: &network.SubnetPropertiesFormat{
							AddressPrefixes: &[]string{"10.0.0.0/16", "fd00::/64"},
						},
					},
				},
			},
		})

	if err != nil {
		return vnet, fmt.Errorf("cannot create virtual network: %v", err)
	}

	err = future.WaitForCompletionRef(ctx, vnetClient.Client)
	if err != nil {
		return vnet, fmt.Errorf("cannot get the vnet create or update future response: %v", err)
	}

	return future.Result(vnetClient)
}
