// SPDX-License-Identifier: MIT

// Copyright (c) Microsoft and contributors.  All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package network

import (
	"context"
	"fmt"

	"github.com/Azure/azure-sdk-for-go/profiles/latest/network/mgmt/network"
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/to"
)

func getIPClient(userAgent, subscriptionID string, authorizer autorest.Authorizer) network.PublicIPAddressesClient {
	ipClient := network.NewPublicIPAddressesClient(subscriptionID)
	ipClient.Authorizer = authorizer
	ipClient.AddToUserAgent(userAgent)
	return ipClient
}

// CreatePublicIP creates a new public IP
func CreatePublicIP(ctx context.Context, userAgent, subscriptionID string, authorizer autorest.Authorizer, resourceGroupName, resourceGroupLocation, ipName string, IPVersion network.IPVersion) (ip network.PublicIPAddress, err error) {
	ipClient := getIPClient(userAgent, subscriptionID, authorizer)
	IPAllocationMethod := network.IPAllocationMethodStatic

	future, err := ipClient.CreateOrUpdate(
		ctx,
		resourceGroupName,
		ipName,
		network.PublicIPAddress{
			Name:     to.StringPtr(ipName),
			Location: to.StringPtr(resourceGroupLocation),
			Sku: &network.PublicIPAddressSku{
				Name: "Standard",
				Tier: "Regional",
			},
			PublicIPAddressPropertiesFormat: &network.PublicIPAddressPropertiesFormat{
				PublicIPAddressVersion:   IPVersion,
				PublicIPAllocationMethod: IPAllocationMethod,
			},
		},
	)

	if err != nil {
		return ip, fmt.Errorf("cannot create public ip address: %v", err)
	}

	err = future.WaitForCompletionRef(ctx, ipClient.Client)
	if err != nil {
		return ip, fmt.Errorf("cannot get public ip address create or update future response: %v", err)
	}

	return future.Result(ipClient)
}
