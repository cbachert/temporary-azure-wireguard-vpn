// SPDX-License-Identifier: MIT

// Copyright (c) Microsoft and contributors.  All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package network

import (
	"context"

	"github.com/Azure/azure-sdk-for-go/profiles/latest/network/mgmt/network"
	"github.com/Azure/go-autorest/autorest"
)

func getSubnetsClient(userAgent, subscriptionID string, authorizer autorest.Authorizer) network.SubnetsClient {
	subnetsClient := network.NewSubnetsClient(subscriptionID)
	subnetsClient.Authorizer = authorizer
	subnetsClient.AddToUserAgent(userAgent)
	return subnetsClient
}

// GetVirtualNetworkSubnet returns an existing subnet from a virtual network
func GetVirtualNetworkSubnet(ctx context.Context, userAgent, subscriptionID string, authorizer autorest.Authorizer, resourceGroupName, resourceGroupLocation, vnetName string, subnetName string) (network.Subnet, error) {
	subnetsClient := getSubnetsClient(userAgent, subscriptionID, authorizer)
	return subnetsClient.Get(ctx, resourceGroupName, vnetName, subnetName, "")
}
