// SPDX-License-Identifier: GPL-3.0-or-later

package wireguard

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"net"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"gitlab.com/cbachert/temporary-azure-wireguard-vpn/template"
)

func GenKeyPair() (privatekey, publickey string) {
	cmdPrivateKey, err := exec.Command(wgPath, "genkey").CombinedOutput()
	if err != nil {
		log.Fatalf("Failed to generate wg private key: %v", err)
	}

	cmdPublicKey := exec.Command(wgPath, "pubkey")
	cmdPublicKey.Stdin = strings.NewReader(string(cmdPrivateKey[:]))
	var cmdPublicKey_Stdout bytes.Buffer
	cmdPublicKey.Stdout = &cmdPublicKey_Stdout

	err = cmdPublicKey.Run()
	if err != nil {
		log.Fatalf("Failed to generate wg public key: %v", err)
	}

	return strings.TrimSuffix(string(cmdPrivateKey[:]), "\n"), strings.TrimSuffix(cmdPublicKey_Stdout.String(), "\n")
}

func WireguardConfig(WgTemplatePath, WgConfigPath, WgAddress4, WgAddress6, WgPeerEndpoint, WgPrivateKey, WgPublicKey, WgPeerPublicKey string) {
	wgConfig := map[string]string{
		"WgAddress4":      WgAddress4,
		"WgAddress6":      WgAddress6,
		"WgPeerEndpoint":  WgPeerEndpoint,
		"WgPrivateKey":    WgPrivateKey,
		"WgPublicKey":     WgPublicKey,
		"WgPeerPublicKey": WgPeerPublicKey,
	}

	// Full tunnel in Linux works with "0.0.0.0/0" / "::/0", Windows needs "0.0.0.0/1, 128.0.0.0/1" / "::/1, 8000::/1"
	switch runtime.GOOS {
	case "windows":
		wgConfig["WgPeerAllowedIPs4"] = "0.0.0.0/1, 128.0.0.0/1"
		wgConfig["WgPeerAllowedIPs6"] = "::/1, 8000::/1"

	case "linux":
		wgConfig["WgPeerAllowedIPs4"] = "0.0.0.0/0"
		wgConfig["WgPeerAllowedIPs6"] = "::/0"
	}

	template.TemplateFile(WgTemplatePath, WgConfigPath, wgConfig)
}

func WgAwaitTunnel(WgServerIP net.IP, WgTimeout int) error {
	log.Printf("Waiting for server to open VPN tunnel")
	portNum := "22"
	maxTries := WgTimeout
	tries := 0
	timeOut := time.Second

	var maxTriesProgressBar strings.Builder
	maxTriesProgressBarScale := 5
	for i := 0; i < (maxTries / maxTriesProgressBarScale); i++ {
		maxTriesProgressBar.WriteString("-")
	}
	fmt.Println(maxTriesProgressBar.String())

	for {
		connStartTime := time.Now()
		conn, err := net.DialTimeout("tcp", WgServerIP.String()+":"+portNum, timeOut)
		if err == nil {
			fmt.Println("!")
			log.Printf("VPN server is reachable via VPN tunnel")
			conn.Close()
			return nil
		}

		// OS (Windows) might not wait for TCP timeout, wait manually if "timeOut" has not passed, yet
		connElapsedTime := time.Since(connStartTime)
		if connElapsedTime < timeOut {
			time.Sleep(timeOut - connElapsedTime)
		}

		if tries%maxTriesProgressBarScale == 0 {
			fmt.Print(".")
		}
		tries++
		if tries >= maxTries {
			fmt.Println("")
			log.Println("VPN server is not reachable")
			return errors.New("VPN server is not reachable")
		}
	}
}
