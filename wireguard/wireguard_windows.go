// SPDX-License-Identifier: GPL-3.0-or-later

package wireguard

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"

	armnetwork "github.com/Azure/azure-sdk-for-go/services/network/mgmt/2021-03-01/network"
	armresources "github.com/Azure/azure-sdk-for-go/services/resources/mgmt/2020-10-01/resources"
)

var (
	wireguardPath = os.Getenv("ProgramFiles") + "\\WireGuard\\wireguard.exe"
	wgPath        = os.Getenv("ProgramFiles") + "\\WireGuard\\wg.exe"
)

func WireguardUp(WgConfigPath string, group armresources.Group, pip armnetwork.PublicIPAddress, wgIfName string) {
	wgIfNamePath, err := filepath.Abs(WgConfigPath)
	if err != nil {
		log.Fatalf("Failed to get wg config full path: %v", err)
	}
	log.Printf("Wg config full path: %v", wgIfNamePath)

	out, err := exec.Command(wireguardPath, "/installtunnelservice", wgIfNamePath).CombinedOutput()
	if err != nil {
		log.Fatalf("Failed to install wg tunnel service: %v", string(out[:]))
	}
	log.Printf("Installed wg tunnel service: %v", wgIfName)
}

func WireguardDown(WgConfigPath string, group armresources.Group, pip armnetwork.PublicIPAddress, wgIfName string) {
	out, err := exec.Command(wireguardPath, "/uninstalltunnelservice", wgIfName).CombinedOutput()
	if err != nil {
		log.Fatalf("Failed to uninstall wg tunnel service: %v", string(out[:]))
	}
	log.Printf("Uninstalled wg tunnel service: %v", wgIfName)
}
