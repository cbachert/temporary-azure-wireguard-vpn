// SPDX-License-Identifier: GPL-3.0-or-later

package wireguard

import (
	"log"
	"os/exec"
	"strings"

	armnetwork "github.com/Azure/azure-sdk-for-go/services/network/mgmt/2021-03-01/network"
	armresources "github.com/Azure/azure-sdk-for-go/services/resources/mgmt/2020-10-01/resources"
)

var (
	wgPath = "wg"
)

func WireguardUp(WgConfigPath string, group armresources.Group, pip armnetwork.PublicIPAddress, wgIfName string) {
	out, err := exec.Command("wg-quick", "up", WgConfigPath).CombinedOutput()
	if err != nil {
		if strings.Contains(string(out[:]), "already exists") {
			log.Printf("wg interface already exists, delete and re-create")
			out, err = exec.Command("wg-quick", "down", WgConfigPath).CombinedOutput()
			if err != nil {
				log.Fatalf("failed to delete wg interface: %v", err)
			}
			log.Printf("Deleted wg interface: %v", wgIfName)

			out, err = exec.Command("wg-quick", "up", WgConfigPath).CombinedOutput()
			if err != nil {
				log.Fatalf("failed to create wg interface: %v", out)
			}
		} else {
			log.Fatalf("failed to create wg interface: %v", string(out[:]))
		}
	}
	log.Printf("Created wg interface: %v", wgIfName)
}

func WireguardDown(WgConfigPath string, group armresources.Group, pip armnetwork.PublicIPAddress, wgIfName string) {
	_, err := exec.Command("wg-quick", "down", WgConfigPath).CombinedOutput()
	if err != nil {
		log.Fatalf("Failed to delete wg interface: %v", err)
	}
	log.Printf("Deleted wg interface: %v", wgIfName)
}
